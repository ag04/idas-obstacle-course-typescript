import { all } from 'redux-saga/effects';
import {calculatePathSaga} from "./grid/sagas";

// Split sagas into smaller sagas...
export function* rootSaga() {
    yield all([
        calculatePathSaga()
    ]);
}