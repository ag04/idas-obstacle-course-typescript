import { StateType } from 'typesafe-actions';
import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import { gridReducer } from './grid/reducers';
import createSagaMiddleware from 'redux-saga';

import { rootSaga } from './rootSaga';

const composeEnhancers = process.env.NODE_ENV === 'development' ? (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ : null || compose;

const rootReducer = combineReducers({
    grid: gridReducer,
});

const sagaMiddleware = createSagaMiddleware();

export type RootState = StateType<typeof rootReducer>;

const store = createStore(
    rootReducer,
    composeEnhancers(
        applyMiddleware(sagaMiddleware),
    ),
);

sagaMiddleware.run(rootSaga);

export default store;