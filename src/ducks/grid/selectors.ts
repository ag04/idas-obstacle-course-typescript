import { RootState } from '../store';

export const getGrid = (state: RootState) => state.grid.grid;
export const getStart = (state: RootState) => state.grid.start;
export const getEnd = (state: RootState) => state.grid.end;
export const getWormholeEntrances = (state: RootState) => state.grid.wormholeEntrances;
export const getWormholeExits = (state: RootState) => state.grid.wormholeExits;
export const getError = (state: RootState) => state.grid.error;

export const showGrid = (state: RootState) => state.grid.showGrid;
export const startSet = (state: RootState) => state.grid.start.startSet;
export const endSet = (state: RootState) => state.grid.end.endSet;
export const pathCalculated = (state: RootState) => state.grid.pathCalculated;