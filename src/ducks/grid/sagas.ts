import { put, takeLatest, all, select, call } from 'redux-saga/effects';
import { gridActions } from './actions';
import { getType } from 'typesafe-actions';
import * as selectors from './selectors';
import { fieldIndices } from './constants';

interface VertexInterface {
    row: number;
    column: number;
    field: number;
    g?: number;
    f?: number;
    parentVertex?: VertexInterface;
    ownWormEntrance?: VertexInterface;
}
interface ErrorInterface {
    error: boolean
}
interface CellInterface {
    row: number;
    column: number;
    field: number;
}
interface StartInterface {
    row: number;
    column: number;
    startSet: boolean;
}
interface EndInterface {
    row: number;
    column: number;
    endSet: boolean;
}

export function calculateGValue(currentVertex: VertexInterface, parentG: number) {
    switch (currentVertex.field) {
        case fieldIndices.gravel:
            return parentG + 2;
        case fieldIndices.wormEntrance:
            return parentG + 1;
        default:
            return parentG + 1;
    }
}
function calculateHValue(currentVertex: VertexInterface | StartInterface | CellInterface, end: EndInterface | CellInterface) {
    return (Math.abs(currentVertex.row - end.row) + Math.abs(currentVertex.column - end.column)); // Manhattan distance from end - H
}
function calculateFValue(currentVertex: VertexInterface, parentG: number, end: EndInterface | CellInterface) { // f value of vertex
    return calculateHValue(currentVertex, end) + calculateGValue(currentVertex, parentG);
}

function getAdjacentVertices(currentVertex: VertexInterface, wormholeExits: CellInterface[], grid: CellInterface[][]) {
    const validVertices: VertexInterface[] = [];
    const adjacentVertices = [
        {row: currentVertex.row + 1, column: currentVertex.column},
        {row: currentVertex.row - 1, column: currentVertex.column},
        {row: currentVertex.row, column: currentVertex.column + 1},
        {row: currentVertex.row, column: currentVertex.column - 1},
    ];
    adjacentVertices.forEach(e => {
        if (grid[e.row] && grid[e.row][e.column]) {
            const cell = {
                row: e.row,
                column: e.column,
                field: grid[e.row][e.column].field,
            };
            if ((cell.row >= 0 && cell.column >= 0) && (cell.field !== fieldIndices.boulder) && (cell.field !== fieldIndices.wormExit)) {
                validVertices.push(cell);
            }
        }
    });
    return validVertices;
}

function findInList(vertex: VertexInterface, list: VertexInterface[]) {
    const indexOpen = list.findIndex(e => e.row === vertex.row && e.column === vertex.column);
    return indexOpen;
}

function calculateShortestPath(
    grid: CellInterface[][],
    start: StartInterface | CellInterface,
    startField: number, end: EndInterface | CellInterface,
    wormholeExits: CellInterface[]): VertexInterface | ErrorInterface {

    const openList: VertexInterface[] = [];
    const closedList: VertexInterface[]  = [];
    let latestVertex;

    let currentVertex: VertexInterface = {
        row: start.row,
        column: start.column,
        field: startField,
        g: 0,
        f: calculateHValue(start, end),
    };
    openList.push(currentVertex);

    while (openList.length !== 0) {
        // check if current vertex is the end
        if (currentVertex.row === end.row && currentVertex.column === end.column) {
            return currentVertex;
        }
        const availableAdjacentVertices = getAdjacentVertices(currentVertex, wormholeExits, grid);
        // for each vertex adjacent to current
        availableAdjacentVertices.forEach(vertex => {
            // if the vertex is on the closed list, ignore it
            const closedListIndex = findInList(vertex, closedList);
            if (closedListIndex === -1) {
                // if the vertex is in the open list already, check if the current path is better than the previous
                // if it is, recalculate it, otherwise skip
                const openListIndex = findInList(vertex, openList);
                if (openListIndex > -1 ) {
                    const vert = openList[openListIndex];
                    const vertexFValue = calculateFValue(vert, currentVertex.g as number, end);
                    if (!vert.f || vert.f > vertexFValue) {
                        vert.f = vertexFValue;
                        vert.g = calculateGValue(vert, currentVertex.g as number);
                        vert.parentVertex = currentVertex;
                    }
                } else {
                    // if the current v isn't in the open list, add it and calculate the values
                    openList.push(vertex);
                    vertex.f = calculateFValue(vertex, currentVertex.g as number, end);
                    vertex.g = calculateGValue(vertex, currentVertex.g as number);
                    vertex.parentVertex = currentVertex;
                }
            }
        });
        // add current vertex to closed list
        openList.splice(openList.findIndex(e => e.row === currentVertex.row && e.column === currentVertex.column), 1);
        closedList.push(currentVertex);
        // remove vertex with lowest f value from open list and make it current
        latestVertex = openList.sort((a, b) =>
            (a.f as number) - (b.f as number),
        )[0];
        currentVertex = latestVertex;
    }
    if (currentVertex && currentVertex.field === fieldIndices.end) {
        return currentVertex;
    } else {
        return {error: true};
    }
}

function comparePaths(
    grid: CellInterface[][],
    start: StartInterface,
    end: EndInterface,
    wormholeEntrances: CellInterface[],
    wormholeExits: CellInterface[]) {

    const pathRegular: VertexInterface | ErrorInterface = calculateShortestPath(grid, start, fieldIndices.start, end, wormholeExits);
    if (wormholeEntrances.length > 0 && wormholeExits.length > 0) { // if there aren't entrances or exits, dont do the calculation
        // calculate paths for all wormhole entrances (from start) and exits (to end) in the grid
        const pathsToWormEntrance = wormholeEntrances.map(entrance =>
            calculateShortestPath(grid, start, fieldIndices.start, entrance, wormholeExits),
        ).filter((path: ErrorInterface) => !path.error);
        const pathsFromWormExit = wormholeExits.map(exit =>
            calculateShortestPath(grid, exit, fieldIndices.wormExit, end, wormholeExits),
        ).filter((path: ErrorInterface) => !path.error);
        // if all possible wormhole vertices return an error (means no path), then return the regular path
        // if they are possible, then see if their path is shorter than the regular path
        if (pathsToWormEntrance.length > 0 && pathsFromWormExit.length > 0) {
            // calculate shortest paths from/to wormholes and the total distance
            const shortestPathToEntrance: VertexInterface = (pathsToWormEntrance as VertexInterface[]).sort((a, b) => a.g! - b.g!)[0];
            const shortestPathFromExit: VertexInterface = (pathsFromWormExit as VertexInterface[]).sort((a, b) => a.g! - b.g!)[0];
            const distanceWormholes: number =  shortestPathToEntrance.g! + shortestPathFromExit.g!;
             // also check if path regular even exists, if it doesnt, return it (it will contain an error and the reducer will handle it)
            if (distanceWormholes < (pathRegular as VertexInterface).g! || (pathRegular as ErrorInterface).error) { // distance from end is the G of the final(end) vertex)
                let obj = shortestPathFromExit;
                while (obj.hasOwnProperty('parentVertex')) {
                    obj = obj.parentVertex as VertexInterface;
                }
                obj.parentVertex = shortestPathToEntrance;
                return shortestPathFromExit
            } else {
                return pathRegular;
            }
        } else {
            return pathRegular;
        }
    } else {
        return pathRegular;
    }
}
function* setPath() {
    const grid = yield select(selectors.getGrid);
    const start = yield select(selectors.getStart);
    const end = yield select(selectors.getEnd);
    const wormholeEntrances = yield select(selectors.getWormholeEntrances);
    const wormholeExits = yield select(selectors.getWormholeExits);
    const shortestPath = yield call(comparePaths, grid, start, end, wormholeEntrances, wormholeExits);
    if (shortestPath.error) {
        yield put(gridActions.pathNotFound({error: 'Path not found'}));
    } else {
        yield put(gridActions.displayShortestPath({shortestPath}));
    }

}
export function* calculatePathSaga() {
    yield all([takeLatest(getType(gridActions.calculatePath), setPath)])
}
