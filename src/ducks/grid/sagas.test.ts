import { calculateGValue } from './sagas';
import { fieldIndices } from "./constants";

test('G value correctly calculated', () => {
    expect(calculateGValue(
        {row: 1, column: 4, field: fieldIndices.gravel}, 4)
    ).toBe(6);
  });