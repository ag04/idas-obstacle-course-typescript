import { produce } from 'immer';
import { gridActions, GridActions } from './actions';
import { getType } from 'typesafe-actions';
import { fieldIndices } from './constants';

interface Cell {
    row: number,
    column: number,
    field: number,
}
interface Wormhole {
    row: number,
    column: number,
}
export interface GridState {
    grid: Cell[][],
    showGrid: boolean;
    start: {
        row: number | null,
        column: number | null,
        startSet: boolean,
    };
    end: {
        row: number | null,
        column: number | null,
        endSet: boolean,
    };
    wormholeExits: Wormhole[],
    wormholeEntrances: Wormhole[],
    shortestPathLength: number,
    pathCalculated: boolean,
    error: null | string,
}
const initialState: GridState = {
    grid: [[]],
    showGrid: false,
    start: {
        row: null,
        column: null,
        startSet: false,
    },
    end: {
        row: null,
        column: null,
        endSet: false,
    },
    wormholeExits: [],
    wormholeEntrances: [],
    shortestPathLength: 0,
    pathCalculated: false,
    error: null,
};
const createGrid = (rows: number, columns: number) =>
    new Array(rows).fill(new Array(columns).fill({field: 0}));
const updateGrid = (state: GridState, row: number, column: number, field: string) => {
    const updatedGrid = state.grid;
    updatedGrid[row][column].field = fieldIndices[field];
    return updatedGrid;
};
const removeField = (state: GridState, row: number, column: number) => {
    const updatedGrid = state.grid;
    updatedGrid[row][column].field = fieldIndices.empty;
    return updatedGrid;
};
const updateStart = (state: GridState, field: any, row: number, column: number) => {
    const startUpdated = {...state.start};
    if ( field === 'start' || field === 2 ) {
        startUpdated.startSet = !startUpdated.startSet;
        startUpdated.row = row;
        startUpdated.column = column;
        return startUpdated;
    }  else {
        return startUpdated;
        }
    };
const updateEnd = (state: GridState, field: any, row: number, column: number) => {
    const endUpdated = {...state.end};
    if ( field === 'end' || field === 3 ) {
        endUpdated.endSet = !endUpdated.endSet;
        endUpdated.row = row;
        endUpdated.column = column;
        return endUpdated;
    }  else {
        return endUpdated;
    }
};

export const gridReducer = produce<GridState, GridActions>(
    (state, action) => {
        switch (action.type) {
            case getType(gridActions.generateGrid):
                state.grid = createGrid(action.payload.rows, action.payload.columns);
                state.showGrid = true;
                state.start.startSet = false;
                state.end.endSet = false;
                state.pathCalculated = false;
                state.wormholeExits = [];
                state.error = null;
                return;
            case getType(gridActions.updateGrid):
                const {row, column, field} = action.payload;
                state.grid =  updateGrid(state, row, column, field);
                if ( field === 'wormExit' ) {
                    state.wormholeExits.push({row, column})
                }
                if ( field === 'wormEntrance' ) {
                    state.wormholeEntrances.push({row, column})
                }
                // update startSet/endSet property and disable/enable the Start/End draggable controls
                state.start = updateStart(state, field, row, column);
                state.end = updateEnd(state, field, row, column);

                return;
            case getType(gridActions.removeField):
                const {row: rowRemove, column: columnRemove} = action.payload;
                const fieldRemove = state.grid[rowRemove][columnRemove].field;
                state.start = updateStart(state, fieldRemove, rowRemove, columnRemove);
                // update startSet/endSet property and disable/enable the Start/End draggable controls
                state.end = updateEnd(state, fieldRemove, rowRemove, columnRemove);
                // state.wormholeExits = removeExits(state, rowRemove, columnRemove);
                if (fieldRemove === fieldIndices.wormExit) {
                    const index = state.wormholeExits.findIndex((e: Cell) => e.row === rowRemove && e.column === columnRemove);
                    if ( index !== -1 ) {
                        state.wormholeExits.splice(index, 1);
                    }
                }
                if (fieldRemove === fieldIndices.wormEntrance) {
                    const index = state.wormholeEntrances.findIndex((e: Cell) => e.row === rowRemove && e.column === columnRemove);
                    if (index !== -1) {
                        state.wormholeEntrances.splice(index, 1);
                    }
                }
                state.grid = removeField(state, rowRemove, columnRemove);
                return;
            case getType(gridActions.displayShortestPath):
                let currentVertex =  action.payload.shortestPath.parentVertex;
                state.pathCalculated = true;
                while (currentVertex && currentVertex.field !== fieldIndices.start) {
                    if (currentVertex.ownWormEntrance) {
                        state.grid[currentVertex.ownWormEntrance.row][currentVertex.ownWormEntrance.column].field = fieldIndices.footprint;
                    }
                    state.grid[currentVertex.row][currentVertex.column].field = fieldIndices.footprint;
                    currentVertex = currentVertex.parentVertex;
                }
                return;
            case getType(gridActions.pathNotFound):
                state.error = action.payload.error;
                return;
            default: return state
        }
    }, initialState,
);