export const fieldIndices = {
    'empty': 0,
    'footprint': 1,
    'start': 2,
    'end': 3,
    'boulder': 4,
    'gravel': 5,
    'wormEntrance': 6,
    'wormExit': 7,
};