import { createStandardAction, ActionType } from 'typesafe-actions';

interface UpdateGridInterface {
    row: number;
    column: number;
    field: string;
}
interface RemoveFieldInterface {
    row: number;
    column: number;
}
interface GridInterface {
    rows: number;
    columns: number;
}
interface ShortestPath {
    row: number;
    column: number;
    field: number;
    parentVertex: ShortestPath;
    ownWormEntrance?: ShortestPath;
}
interface ShortestPathWithoutError {
    shortestPath: ShortestPath;
}
interface ShortestPathWithError {
    error: string;
}
export const gridActions = {
    generateGrid: createStandardAction('GENERATE_GRID')<GridInterface>(),
    updateGrid: createStandardAction('UPDATE_GRID')<UpdateGridInterface>(),
    removeField: createStandardAction('REMOVE_FIELD')<RemoveFieldInterface>(),
    calculatePath: createStandardAction('CALCULATE_PATH')(),
    displayShortestPath: createStandardAction('DISPLAY_SHORTEST_PATH')<ShortestPathWithoutError>(),
    pathNotFound: createStandardAction('PATH_NOT_FOUND')<ShortestPathWithError>(),
};

export type GridActions = ActionType<typeof gridActions>;