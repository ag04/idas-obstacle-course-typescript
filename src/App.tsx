import * as React from 'react';
import { Provider } from 'react-redux';
import store from './ducks/store';

import './App.css';
import Game from './containers/Game/Game';

class App extends React.Component {
  render() {
    return (
      <Provider store={store}>
          <div className="App">
            <Game/>
          </div>
        </Provider>
    );
  }
}

export default App;
