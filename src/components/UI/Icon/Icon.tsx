import styled from 'styled-components';
// Images
import Start from '../../../assets/Images/start.svg';
import End from '../../../assets/Images/end.svg';
import Boulder from '../../../assets/Images/boulder.svg';
import Gravel from '../../../assets/Images/gravel.svg';
import WormholeEntrance from '../../../assets/Images/wormhole-entrance.svg';
import WormholeExit from '../../../assets/Images/wormhole-exit.svg';
import Footprint from '../../../assets/Images/footprint.svg'

interface IconProps {
    field: number;
}
export const Icon = styled.div<IconProps>`
    width: 100%;
    height: 100%;
    background-repeat: no-repeat;
    background-image: url(${
    ({field}) => {
        switch (field) {
            case 0:
                return 'none';
            case 1:
                return Footprint;
            case 2:
                return Start;
            case 3:
                return End;
            case 4:
                return Boulder;
            case 5:
                return Gravel;
            case 6:
                return WormholeEntrance;
            case 7:
                return WormholeExit;
            default:
                return 'none'
        }
    }})
`;
