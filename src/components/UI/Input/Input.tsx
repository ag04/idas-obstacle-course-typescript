import * as React from 'react';
import styled from 'styled-components';

const InputContainer = styled.div`
`;
interface InputFieldInterface{
    type: string;
    min: number;
    max: number;
    value: number;
    name: string;
    onChange: (e: React.SyntheticEvent<HTMLInputElement>) => void;
}
const InputField = styled.input<InputFieldInterface>`
    box-sizing: border-box;
    outline: none;
    border: 1px solid #ccc;
    background-color: white;
    font: inherit;
    padding: 6px 10px;
    margin: 0.3rem 1.3rem;
    width: ${
    ({ type }) => {
        switch (type) {
            case 'number':
                return '4rem';
            default:
                return '100%';
        }
    }};
`;
const Label = styled.label`
    font-weight: 600;
    display: block;
    font-size: 0.6rem;
`;
interface InputProps {
    label: string;
    minValue: number;
    maxValue: number;
    value: number;
    type: string;
    name: string;
    change: (e: React.SyntheticEvent<HTMLInputElement>) => void;
}
const Input: React.SFC<InputProps> = (props) => (
        <InputContainer>
            <Label>{props.label}</Label>
            <InputField
                min={props.minValue}
                max={props.maxValue}
                value={props.value}
                type={props.type}
                name={props.name}
                onChange={props.change}
            />
        </InputContainer>
);

export default Input;
