import styled from 'styled-components';

interface ButtonProps {
    btnType: string;
}
const Button = styled.button<ButtonProps>`
    border: none;
    color: white;
    outline: none;
    cursor: pointer;
    font: inherit;
    padding: 10px;
    width: auto;
    margin: 0 auto;
    font-weight: bold;
    border-radius: 8px;
    text-transform: uppercase;
    background-color: ${
    ({ btnType }) => {
        switch (btnType) {
            case 'Success':
                return '#5C9210';
            case 'Primary':
                return '#08A4BD';
            case 'Danger':
                return '#944317';
            default:
                return 'transparent';
        }
    }};
    &:disabled {
      background-color: #95C2C9;
      cursor: not-allowed;
    }
`;

export default Button;
