import * as React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { Icon } from '../../components/UI/Icon/Icon';
import * as selectors from '../../ducks/grid/selectors';
import { gridActions } from '../../ducks/grid/actions';
import { RootState } from '../../ducks/store';

const Table = styled.table`
    border-collapse: collapse;
    margin: 2rem auto;
`;
const Error = styled.p`
    color: red;
    margin-top: 0;
    font-weight: bold;
`;
const TableRow = styled.tr`
`;
interface CellProps {
    onDragOver: (e: React.DragEvent) => void;
    onDrop: (e: React.DragEvent) => void;
    onContextMenu: (e: React.MouseEvent) => void;
}
const Cell = styled.td<CellProps>`
    width:3rem;
    height: 3rem;
    border: 1px solid #ccc;
`;
class Grid extends React.Component<StateProps & ActionProps> {

    onDragOver = (event: React.DragEvent<HTMLDivElement>) => {
        event.preventDefault();
        event.stopPropagation();
    };
    onDrop = (indexRow: number, indexColumn: number) => (event: React.DragEvent<HTMLDivElement>) => {
        this.props.onUpdateGrid({row: indexRow, column: indexColumn, field: event.dataTransfer.getData('field') });
    };
    onRemoveField = (indexRow: number, indexColumn: number) => (event: React.MouseEvent) => {
        event.preventDefault();
        this.props.onRemoveField({row: indexRow, column: indexColumn });
    };

    render() {
        if (this.props.showGrid) {
            return (
                <>
                    <Table>
                        <tbody>
                            { this.props.grid.map((row, indexRow) =>
                                <TableRow key={indexRow}>
                                    {row.map((column, indexColumn) =>
                                        <Cell
                                            key={indexRow + indexColumn}
                                            onDragOver={this.onDragOver}
                                            onDrop={this.onDrop(indexRow, indexColumn)}
                                            onContextMenu = {this.onRemoveField(indexRow, indexColumn)}>
                                            <Icon field={column.field}/>
                                        </Cell>,
                                    )}
                                </TableRow>,
                            )}
                        </tbody>
                    </Table>
                    { this.props.error ? <Error>Path not found!</Error> : null }
                </>
            )
        }
        return null;

    }
}

const mapStateToProps = (state: RootState) => ({
        showGrid: selectors.showGrid(state),
        grid: selectors.getGrid(state),
        error: selectors.getError(state),
    }
);
type StateProps = ReturnType<typeof mapStateToProps>;

const mapDispatchToProps = {
    onUpdateGrid: gridActions.updateGrid,
    onRemoveField: gridActions.removeField,
};
type ActionProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Grid);