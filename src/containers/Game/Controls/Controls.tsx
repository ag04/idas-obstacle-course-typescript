import * as React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';

import * as selectors from '../../../ducks/grid/selectors';
import { gridActions } from '../../../ducks/grid/actions';

import Button from '../../../components/UI/Button/Button';

import { RootState } from 'src/ducks/store';
import { Icon } from 'src/components/UI/Icon/Icon';

const ControlsContainer = styled.div`
    width: fit-content;
    margin: 1rem auto;
    display: flex;
`;
interface ControlProps {
    onDragStart: (event: React.DragEvent<HTMLDivElement>) => void;
    disabled?: boolean;
    draggable: boolean;
}
const Control = styled.div<ControlProps>`
    height: 2rem;
    width: 2rem;
    border: 2px solid #ccc;
    margin: 0.5rem;
    padding: 5px;
    &[disabled] {
      cursor: not-allowed;
      svg {
        fill:#ccc;
      }
    }
`;

class Controls extends React.Component<StateProps & ActionProps, {}> {

    onDragStart = (field: string) => (event: React.DragEvent<HTMLDivElement>) => {
        event.dataTransfer.setData('field', field);
    };
    render() {
        if (this.props.showGrid) {
            return (
                <>
                    <Button
                        btnType="Success"
                        disabled={!(this.props.startSet && this.props.endSet) || this.props.pathCalculated}
                        onClick={this.props.onCalculatePath}
                    >Calculate path</Button>
                    <ControlsContainer>
                        <Control
                            draggable = {!this.props.startSet}
                            onDragStart = {this.onDragStart('start')}
                            disabled = {this.props.startSet}
                        ><Icon field={2}/></Control>
                        <Control
                            draggable = {!this.props.endSet}
                            onDragStart = {this.onDragStart('end')}
                            disabled = {this.props.endSet}
                        ><Icon field={3}/></Control>
                        <Control
                            draggable
                            onDragStart = {this.onDragStart('boulder')}
                        ><Icon field={4}/></Control>
                        <Control
                            draggable
                            onDragStart = {this.onDragStart('gravel')}
                        ><Icon field={5} /></Control>
                        <Control
                            draggable
                            onDragStart = {this.onDragStart('wormEntrance')}
                        ><Icon field={6}/></Control>
                        <Control
                            draggable
                            onDragStart = {this.onDragStart('wormExit')}
                        ><Icon field={7} /></Control>
                    </ControlsContainer>
                </>
            );
        }
        else {
            return null;
        }
    }
}
const mapStateToProps = (state: RootState, ownProps: {}) =>
    ({
        showGrid: selectors.showGrid(state),
        startSet: selectors.startSet(state),
        endSet: selectors.endSet(state),
        pathCalculated: selectors.pathCalculated(state),
    });
type StateProps = ReturnType<typeof mapStateToProps>;

const mapDispatchToProps = {
    onCalculatePath: gridActions.calculatePath,
};

type ActionProps = typeof mapDispatchToProps;

export default connect(mapStateToProps, mapDispatchToProps)(Controls);