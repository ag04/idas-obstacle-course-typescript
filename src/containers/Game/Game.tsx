import * as React from 'react';
import StartGame from '../StartGame/StartGame';
import Grid from '../Grid/Grid';
import Controls from './Controls/Controls';

interface GameState {
    rows: number | null,
    columns: number | null,
}
class Game extends React.Component <{}, GameState> {
    state: GameState = {
        rows: null,
        columns: null,
    };

    initiateGrid = () => ((r: number, c: number) => {
        this.setState({
            rows: r,
            columns: c,
        });
    });

    render() {
        return (
            <div>
                <StartGame/>
                <Grid/>
                <Controls/>
            </div>
        );
    }
}

export default Game;