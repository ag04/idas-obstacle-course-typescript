import * as React from 'react';
import styled from 'styled-components';

import Button from '../../components/UI/Button/Button';
import Input from '../../components/UI/Input/Input';
import { connect } from 'react-redux';
import { gridActions } from '../../ducks/grid/actions';

const StartGameContainer = styled.div`
  margin: 0 auto;
  display: flex;
  background-color: #B2675E;
  color: white;
  font-weight: 400;
  margin-bottom: 30px;
  padding: 1rem;
`;
class StartGame extends React.Component<ActionProps>  {
    state = {
        columns: 8,
        rows: 8,
    };

    createGrid = () => {
        this.props.onGenerateGrid({rows: this.state.rows, columns: this.state.columns})
    };

    rowSizeHandler = (event: React.SyntheticEvent<HTMLInputElement>) => {
        this.setState({
            rows: Number(event.currentTarget.value),
        })
    };
    columnSizeHandler = (event: React.SyntheticEvent<HTMLInputElement>) => {
        this.setState({
            columns: Number(event.currentTarget.value),
        })
    };

    render () {
        return (
            <StartGameContainer>
                <h3 style={{width: '60%'}}>
                    Select grid size
                </h3>
                <Input
                    type="number"
                    maxValue={15}
                    minValue={4}
                    value={this.state.rows}
                    name="rows"
                    label="Select no. of rows"
                    change={this.rowSizeHandler}/>
                <Input
                    maxValue={15}
                    minValue={4}
                    type="number"
                    value={this.state.columns}
                    name="columns"
                    label="Select no. of columns"
                    change={this.columnSizeHandler}/>
                <Button btnType="Primary" onClick={this.createGrid}>
                    Create grid
                </Button>
            </StartGameContainer>
        );
    }
}

const mapDispatchToProps = {
    onGenerateGrid: gridActions.generateGrid,
};

type ActionProps = typeof mapDispatchToProps;

export default connect(null, mapDispatchToProps)(StartGame);
